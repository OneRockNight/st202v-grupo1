/*
 * MergeSort.cpp
 *
 *  Created on: 9 abr. 2018
 *      Author: Computer
 */

#include "MergeSort.h"

#include <iostream>
#include <string>
#include "../Estructuras/Estructuras.hpp"

using namespace std;


//Referencia: https://www.sanfoundry.com/cpp-program-implement-merge-sort/


void Merge(float A[][4], int b1, int bn, int mid2){
	int ini = b1;
	int fin = bn;
	int med = mid2 + 1;
	float sorted[fin - ini + 1][4];
	int i = 0;
	while ((ini <= mid2)&& (med <= fin)){
		if (A[ini][2] < A[med][2]){
			sorted[i][0] = A[ini][0];
			sorted[i][1] = A[ini][1];
			sorted[i][2] = A[ini][2];
			sorted[i][3] = A[ini][3];
			i++;
			ini++;
		}else{
			sorted[i][0] = A[med][0];
			sorted[i][1] = A[med][1];
			sorted[i][2] = A[med][2];
			sorted[i][3] = A[med][3];
			i++;
			med++;
		}
	}
	while (ini <= mid2){
		sorted[i][0] = A[ini][0];
		sorted[i][1] = A[ini][1];
		sorted[i][2] = A[ini][2];
		sorted[i][3] = A[ini][3];
		ini++;
		i++;
	}
	while (med <= fin){
		sorted[i][0] = A[med][0];
		sorted[i][1] = A[med][1];
		sorted[i][2] = A[med][2];
		sorted[i][3] = A[med][3];
		med++;
		i++;
	}
	for (int j = b1; j <= bn; j++){
		A[j][0] = sorted[j - b1][0];
		A[j][1] = sorted[j - b1][1];
		A[j][2] = sorted[j - b1][2];
		A[j][3] = sorted[j - b1][3];
	}
}

void Merge_sort(float A[][4], int a1, int an){
	int mid;
	if (a1 < an){
		mid = (an+a1)/2;
		Merge_sort (A, a1, mid);
		Merge_sort (A, mid + 1, an);
		Merge (A, a1, an, mid);
	}
}


void MergeSort(float A[][4], int n) {
	Merge_sort(A, 0, n-1);
	for (int i = 0; i < n; i++){
		cout << "Asiento: " << A[i][0] << " Zona: " << ZonaInv(A[i][1]) << " Precio: " << A[i][2] << endl;
		if (A[i][3] == 0){
			cout << "Asiento Disponible" << endl;
		}
		if (A[i][3] == 1){
			cout << "Asiento no disponible" << endl;
		}
	}
}
