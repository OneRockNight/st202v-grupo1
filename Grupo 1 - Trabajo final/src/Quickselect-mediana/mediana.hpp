/*
 * mediana.hpp
 *
 *  Created on: 25 abr. 2018
 *      Author: user
 */

#ifndef QUICKSELECT_MEDIANA_MEDIANA_HPP_
#define QUICKSELECT_MEDIANA_MEDIANA_HPP_

void medianaselect();
int quick_select(int* arreglo, int p, int r, int k);
int partition(int* arreglo, int p, int r);



#endif /* QUICKSELECT_MEDIANA_MEDIANA_HPP_ */
