/*
 * Metododebiseccion.cpp
 *
 *  Created on: 9 abr. 2018
 *      Author: Computer
 */

#include "Metododebiseccion.h"
#include "../MergeSort/MergeSort.h"
#include "../Estructuras/Estructuras.hpp"

#include<iostream>
#include<cmath>
#include <stdlib.h>
#include <time.h>

using namespace std;

float Filtrados[3000][4];
int contador;

double f(double x,double k) {
	double ans = (150-1000)/(sqrt((80*80)+(25*25))-sqrt((25*25)+(10*10)))*(x-sqrt((25*25)+(10*10)))+1000-k;
	return ans;
}

double precio(double x) {
	double ans = (150-1000)/(sqrt((80*80)+(25*25))-sqrt((25*25)+(10*10)))*(x-sqrt((25*25)+(10*10)))+1000;
	return ans;
}

void Metododebiseccion() {
	int i,j,radioMIN,radioMAX,precio1;
	string lugar;
	double  r;
	double k,kMIN,kMAX;
	cout.precision(4);
	string valor="null";
	double x1=0;
	double x2=0;
	double xr=0;
	double acc=0;
	double fx1=0;
	double fx2=0;
	double fxr=0;
	double xrold=0;

	xr = 0;
	int iter = 0;
	acc = 0.000001;
	string s;

	x1=sqrt((80*80)+(25*25));
	x2=sqrt((25*25)+(10*10));
	cout<<"Introduzca Precio Minimo(Comprendido entre 150 y 1000)";
	cin>>kMIN;
	cout<<"Introduzca Precio Maximo(Comprendido entre 150 y 1000)";
	cin>>kMAX;
	cout<<"Introduzca Zona";
	cin>>s;
	do {
		xrold = xr;
		xr = (x1 + x2) / 2.0;
		fx1 = f(x1,kMIN);
		fx2 = f(x2,kMIN);
		fxr = f(xr,kMIN);
		iter++;
		if (fx1*fxr>0){
			x1 = xr;
		}
		else{
			x2 = xr;
		}
	} while (fabs(fxr*fx1)>acc);

	radioMAX=(int)xr/1;
	cout<<"radioMAX: "<<radioMAX<<endl;

	xrold=0;
	xr = 0;
	x1=sqrt((80*80)+(25*25));
	x2=sqrt((25*25)+(10*10));

	do {
		xrold = xr;
		xr = (x1 + x2) / 2.0;
		fx1 = f(x1,kMAX);
		fx2 = f(x2,kMAX);
		fxr = f(xr,kMAX);
		iter++;
		if (fx1*fxr>0){
			x1 = xr;
		}
		else{
			x2 = xr;
		}
	} while (fabs(fxr*fx1)>acc);

	radioMIN=(int)xr/1;
	cout<<"radioMIN "<<radioMIN<<endl;
	int cont = 0;
	for(int n=1;n<3001;n++){
		if(n<=1000){
			i=n%20;
			if(i==0){
				i=20;
			}
			j=(n+19)/20;
			r=sqrt(pow(25+i,2)+pow(10+j,2));
			lugar="ORIENTE";
			}

		if(1000<n && n<=1500){
			i=(n-1000+19)/20;
			j=(n-1000)%20;
			if(j==0){
				j=20;
			}
			r=sqrt(pow(25-i,2)+pow(60+j,2));
			lugar="SUR";
			}
		if(1500<n && n<=2000){
				i=(n-1500+19)/20;
				j=(n-1500)%20;
				if(j==0){
					j=20;
				}
				r=sqrt(pow(i,2)+pow(60+j,2));
				lugar="SUR";
			}

		    if(2000<n && n<=3000){
		    	i=(n-2000)%20;
		    	j=(n-2000+19)/20;
		    	if(i==0){
		    		i=20;
		    	}
		    	r=sqrt(pow(25+i,2)+pow(60-j,2));
		    	lugar="OCCIDENTE";
		    }
		    k=precio(r);
		    precio1=(int)k/1;
			if(radioMIN<=r && r<=radioMAX && s==lugar){
				Filtrados[cont][0] = n;
				if (lugar == "ORIENTE"){
					Filtrados[cont][1] = 0;
				}
				if (lugar == "OCCIDENTE"){
					Filtrados[cont][1] = 1;
				}
				if (lugar == "SUR"){
					Filtrados[cont][1] = 2;
				}
				Filtrados[cont][2] = precio1;
				cont++;
				}
			}
	contador = cont;
	MergeSort(Filtrados, cont);
}

void Imprimir_Filtrados(float A[][4]){
	for (int i = 0; i < contador; i++){
		cout << "Asiento: "<< Filtrados[i][0] << " Zona: " << ZonaInv(Filtrados[i][1]) << " Precio: " << Filtrados[i][2] << endl;
		if (A[i][3] == 0){
				cout << "Asiento disponible" << endl;
		}else{
			cout << "Asiento no disponible" << endl;
		}
	}
}