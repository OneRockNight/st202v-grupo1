/*
 * MergeSort1.cpp
 *
 *  Created on: 22 abr. 2018
 *      Author: Computer
 */

#include "MergeSort1.h"

//Fuente: https://www.sanfoundry.com/cpp-program-implement-merge-sort/

#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <ctime>

using namespace std;

void Merg(int *a, int low, int high, int mid){
	int i, j, k, temp[high-low+1];
	i = low;
	k = 0;
	j = mid + 1;
	while (i <= mid && j <= high){
		if (a[i] < a[j]){
			temp[k] = a[i];
			k++;
			i++;
		}
		else{
			temp[k] = a[j];
			k++;
			j++;
		}
	}
	while (i <= mid){
		temp[k] = a[i];
		k++;
		i++;
	}
	while (j <= high){
		temp[k] = a[j];
		k++;
		j++;
	}
	for (i = low; i <= high; i++){
		a[i] = temp[i-low];
	}
}


void MerSort(int *a, int low, int high){
	int mid;
	if (low < high){
		mid=(low+high)/2;
		MerSort(a, low, mid);
		MerSort(a, mid+1, high);

		Merg(a, low, high, mid);
	}
}

int MergeSort1(){
	clock_t _inicio = clock();
	int n, i;
	cout<<"Ingrese el n�mero de elementos a ordenar" << endl;
	cin>>n;

	int arr[n];
	for(i = 0; i < n; i++)
	{
		arr[i] = rand()%100000;
	}

	MerSort(arr, 0, n-1);

	cout<<"Elementos ordenados: ";
	for (i = 0; i < n; i++){
        cout<<arr[i] << endl;
	}
	cout << endl;
	cout << "Time de ejecucion: " << (clock() - _inicio) << " nanosegundos";

	return 0;
}

