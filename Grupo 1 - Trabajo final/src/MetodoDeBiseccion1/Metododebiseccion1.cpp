#include "Metododebiseccion1.h"
#include<iostream>
#include<cmath>
#include <stdlib.h>
#include <time.h>

using namespace std;

int random()
{
    int num;

           num = -100 + rand() % (201);
    return num;
}

double f(double x) {
	double ans = x*x-4;
	return ans;
}

void Metododebiseccion1() {
	cout.precision(4);
	string valor="null";
	double x1=0;
	double x2=0;
	double xr=0;
	double acc=0;
	double fx1=0;
	double fx2=0;
	double fxr=0;
	double ea=0;
	double xrold=0;

	xr = 0;
	int iter = 0;
	do{
		cout<<"�Tiene valores x1 y x2 por ingresar(sabiendo que f(x1) y f(x2) son de signos opuestos)?(si o no)";
		cin>>valor;
		if(valor=="si"){
			do{
				cout << "Ingrese los datos iniciales:\nx1=";
				cin >> x1;
				cout << "\nx2=";
				cin >> x2;
				fx1=f(x1);
				fx2=f(x2);
					if(fx1*fx2>0){
						cout<<"Introduzca otros valores de x1 y x2 tal que f(x1) y f(x2) tengan signos opuestos\n";
					}
			}while(fx1*fx2>0);
		}
		else {
			do{
				if(valor=="no"){
					x1=random();
					x2=random();
					fx1=f(x1);
					fx2=f(x2);
				}
			}while(fx1*fx2>0);
		}
	}while(valor!="si"&& valor!="no");
	acc = 0.000001;

	if(fx1==0 || fx2==0){
		xr=(x1+x2)-(x1*fx1+x2*fx2)/(fx1+fx2);
	}
	else{

		do {
			xrold = xr;
			xr = (x1 + x2) / 2.0;
			fx1 = f(x1);
			fx2 = f(x2);
			fxr = f(xr);
			iter++;
			ea = fabs(((xr - xrold) / (xr)) * 100);
			cout << "x1=" << x1 << "     x2=" << x2 << "     xr=" << xr << "     fxr=" << fxr << "     ea= " << ea << endl;
				if (fx1*fxr>0){
					x1 = xr;
				}
				else{
					x2 = xr;
				}
			} while (fabs(fxr*fx1)>acc);
	}
	cout << "La ra�z de la ecuaci�n es " << xr << endl;
	cout << "El error aproximado es " << ea << endl;

}
