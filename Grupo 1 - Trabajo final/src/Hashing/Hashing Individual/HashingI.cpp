/*
 * HashingI.cpp
 *
 *  Created on: 22 abr. 2018
 *      Author: Amd Athlon
 */

#include <iostream>
#include <string>
#include <ctime>
using namespace std;

//Inicializando Valores
int antiColisionesI[10000][4];


char C1 [4];
char C2 [4];
char C3 [4];
char C4 [4];

int Num1=0;
int Num2=0;
int Num3=0;
int Num4=0;

int N;


/*cout<<"Cuantas tarjetas desea leer"<<endl;
cin>>N;
cin>>creditCard;*/

void HashI1(string creditCard){

	//Reparto el string

	string colision01=creditCard.substr(0,4);
	string colision02=creditCard.substr(4,4);
	string colision03=creditCard.substr(8,4);
	string colision04=creditCard.substr(12,4);

	//Pasar de string a char

	for (int i = 0; i < 4; i++){
		C1[i] = colision01[i];
	}
	for (int i = 0; i < 4; i++){
		C2[i] = colision02[i];
	}
	for (int i = 0; i < 4; i++){
		C3[i] = colision03[i];
	}
	for (int i = 0;  i < 4; i++){
		C4[i] = colision04[i];
	}
}

//Pasar de char a int

void TransformarI(){
	Num1 = (C1 [0] - '0')*1000 + (C1 [1] -'0')*100 + (C1[2] - '0')*10 + (C1[3] - '0');
	Num2 = (C2 [0] - '0')*1000 + (C2 [1] -'0')*100 + (C2[2] - '0')*10 + (C2[3] - '0');
	Num3 = (C3 [0] - '0')*1000 + (C3 [1] -'0')*100 + (C3[2] - '0')*10 + (C3[3] - '0');
	Num4 = (C4 [0] - '0')*1000 + (C4 [1] -'0')*100 + (C4[2] - '0')*10 + (C4[3] - '0');
}

//insertar en el  arreglo

void HashI2(int n, string creditCard){
	HashI1(creditCard);
	TransformarI();
	bool s = true;
	int i = 0;
	while (s){
		if(antiColisionesI[i][0]==0){
			antiColisionesI[0][0]= Num1;
			antiColisionesI[0][1]= Num2;
			antiColisionesI[0][2]= Num3;
			antiColisionesI[0][3]= Num4;
			cout<<"Primera tarjeta reconocida."<<endl;
			break;
		}else{
			if (Num1 == antiColisionesI[i][0] && Num2 == antiColisionesI [i][1] && Num3 == antiColisionesI [i][2] && Num4 == antiColisionesI [i][3]){
				cout<<"Tarjeta ya existente."<<endl;
				break;
			}else{
				if (antiColisionesI[i+1][0] == 0){
					antiColisionesI[i+1][0] = Num1;
					antiColisionesI[i+1][1] = Num2;
					antiColisionesI[i+1][2] = Num3;
					antiColisionesI[i+1][3] = Num4;
					cout<<"Tarjeta nueva."<<endl;
					break;
				}
			}
		}
		i++;
	}
}


//Hashing Individual
int HashingZAWARDOI(){
	clock_t _inicio = clock();
	int n;
	string creditCard;
	cout<<"Cuantas tarjetas desea leer" << endl;
	cin>>n;
	for (int i = 0; i < n; i++){
		cin>>creditCard;
		HashI2(n,creditCard);
	}
	cout << endl;
	cout << "Time de ejecucion: " << (clock() - _inicio) << " nanosegundos";

	return 0;
}

