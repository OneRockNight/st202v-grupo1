/*
 * hashing.cpp
 *
 *  Created on: 15 abr. 2018
 *      Author: Amd Athlon
 */

#include <iostream>
#include <string>
using namespace std;

int antiColisiones[10000][5];

char c01 [4];
char c02 [4];
char c03 [4];
char c04 [4];

int N01=0;
int N02=0;
int N03=0;
int N04=0;

void Hash1(string creditCard){
	//Recibo string
	//cin>>creditCard;
	//Reparto el string
	string colision01=creditCard.substr(0,4);
	string colision02=creditCard.substr(4,4);
	string colision03=creditCard.substr(8,4);
	string colision04=creditCard.substr(12,4);
	//Pasar de string a char
	for (int i = 0; i < 4; i++){
		c01[i] = colision01[i];
	}
	for (int i = 0; i < 4; i++){
		c02[i] = colision02[i];
	}
	for (int i = 0; i < 4; i++){
		c03[i] = colision03[i];
	}
	for (int i = 0;  i < 4; i++){
		c04[i] = colision04[i];
	}
}
//Pasar de char a int

void Transformar(){
	N01 = (c01 [0] - '0')*1000 + (c01 [1] -'0')*100 + (c01[2] - '0')*10 + (c01[3] - '0');
	N02 = (c02 [0] - '0')*1000 + (c02 [1] -'0')*100 + (c02[2] - '0')*10 + (c02[3] - '0');
	N03 = (c03 [0] - '0')*1000 + (c03 [1] -'0')*100 + (c03[2] - '0')*10 + (c03[3] - '0');
	N04 = (c04 [0] - '0')*1000 + (c04 [1] -'0')*100 + (c04[2] - '0')*10 + (c04[3] - '0');
}

//insertar en el  arreglo

void Hash2(int n, string creditCard){
	Hash1(creditCard);
	Transformar();
	bool s = true;
	int i = 0;
	while (s){
		if(antiColisiones[i][0]==0){
			antiColisiones[i][0]= N01;
			antiColisiones[i][1]= N02;
			antiColisiones[i][2]= N03;
			antiColisiones[i][3]= N04;
			antiColisiones[i][4] = n;
			cout<<"Primera tarjeta reconocida. Procede la compra."<<endl;
			break;
		}else{
			if (N01== antiColisiones[i][0] && N02 == antiColisiones [i][1] && N03 == antiColisiones [i][2] && N04 == antiColisiones [i][3]){
				cout<<"Tarjeta ya existente."<<endl;
				if (antiColisiones[i][4] + n <= 4){
					cout << "Procede la compra";
					antiColisiones[i][4] += n;
				}else{
					cout << "No procede la compra";
				}
				break;
			}else{
				if (antiColisiones[i+1][0] == 0){
					antiColisiones[i+1][0] = N01;
					antiColisiones[i+1][1] = N02;
					antiColisiones[i+1][2] = N03;
					antiColisiones[i+1][3] = N04;
					antiColisiones[i+1][4] = n;
					break;
				}
			}
		}
		i++;
	}
}
