/*
 * QuickSelect.cpp
 *
 *  Created on: 9 abr. 2018
 *      Author: Computer
 */

#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <ctime>
using namespace std;

int partition(int* arreglo, int p, int r)
{
    int pivot = arreglo[r];

    while ( p < r )
    {
        while ( arreglo[p] < pivot )
            p++;

        while ( arreglo[r] > pivot )
            r--;

        if ( arreglo[p] == arreglo[r] )
            p++;
        else if ( p < r ) {
            int aux = arreglo[p];
            arreglo[p] = arreglo[r];
            arreglo[r] = aux;
        }
    }

    return r;
}

int quick_select(int* arreglo, int p, int r, int k)
{
    if ( p == r )
    	return arreglo[p];
    int j = partition(arreglo, p, r);
    int length = j - p + 1;
    if ( length == k )
    	return arreglo[j];
    else if ( k < length )
    	return quick_select(arreglo, p, j - 1, k);
    else  return quick_select(arreglo, j + 1, r, k - length);
}


void QuickSelect(){
	clock_t _inicio = clock();
		int n, i,k;
	    cout<<"¿Cuantos elementos tendra su arreglo ? ";
	    cin>>n;
		int numero;

		int arreglo[n];
		for(i = 0; i < n; i++)
		{
			arreglo[i] = rand()%100000;
		}
	    cout<<"Ingrese el k-termino que desea buscar del arreglo "<<endl;
	    cin>>k;
		numero=quick_select(arreglo,0,n-1,k);
		cout<<"El "<<k<<" menor elemento del arreglo es:  "<<numero<<endl;
		cout << "Time de ejecucion: " << (clock() - _inicio) << " nanosegundos";

}

