//============================================================================
// Name        : Grupo.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <string>
#include "MetodoDeBiseccion/Metododebiseccion.h"
#include "MetodoDeBiseccion1/Metododebiseccion1.h"
#include "QuickSelect1/Quick1.hpp"
#include "QuickSelect/QuickSelect.h"
#include "Hashing/Fake hashing/hashing.hpp"
#include "Hashing/Hashing Individual/HashingI.hpp"
#include "Estructuras/Estructuras.hpp"
#include "MergeSort/MergeSort.h"
#include "MergeSort1/MergeSort1.h"


using namespace std;

float Asientos[5000][4];

int main() {
	cout << "Saludos. �Qu� desea hacer?" << endl;
	cout << "[1] Merge Sort" << endl;
	cout << "[2] Metodo de Bisecci�n" << endl;
	cout << "[3] Quick Select" << endl;
	cout << "[4] Hashing" << endl;
	cout << "[5] Trabajo Grupal" << endl;
	int n1;
	cin >> n1;
	if (n1 == 1){
		cout << "*******MERGE SORT*******"<< endl;
		MergeSort1();
		return 0;
	}
	if (n1 == 2){
		cout << "**METODO DE BISECCION**" << endl;
		Metododebiseccion1();
		return 0;
	}
	if (n1 == 3){
		cout << "******QUICK SELECT******" << endl;
		QuickSelect();
		return 0;
	}
	if (n1 == 4){
		cout << "********HASHING********" << endl;
		HashingZAWARDOI();
		return 0;
	}
	int n;
	cout << "***Compra de asientos para VIVOXROCK***" << endl;
	cout << "Introduzca los datos del concierto" << endl;
	cout << "Introduzca el numero de asientos" << endl;
	cin >> n;
	IntroduccionDatos(Asientos, n);
	bool re = true;
	int cont = 0;
	while (re){
		Metododebiseccion();
		void Imprimir_Filtrados(float A[][4]);
		//MergeSort se ubica dentro del metodo de bisecci�n
		int n_asientos;
		cout << "�Cu�ntos asientos desea comprar? Recuerde que solo puede comprar 4 como m�ximo" << endl;
		cin >> n_asientos;
		int asiento;
		for (int  i = 0; i < n_asientos; i++){
			cout << "Escriba el asiento " << i + 1 << " que desee" << endl;
			cin >> asiento;
			Asientos[asiento][3] = 1;
		}
		cout << "Escribe su numero de tarjeta" << endl;
		string creditcard;
		cin >> creditcard;
		Hash2(cont, creditcard);
		cout << "�Desea hacer otra compra? (Escriba s si es S�, n en caso No)";
		char s10;
		cin >> s10;
		if (s10 == 's'){
			re = true;
		}else{
			re = false;
		}
		cont++;
	}
	return 0;
}
