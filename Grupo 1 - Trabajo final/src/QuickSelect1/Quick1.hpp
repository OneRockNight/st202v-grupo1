/*
 * Quick1.hpp
 *
 *  Created on: 18 abr. 2018
 *      Author: user
 */

#ifndef QUICKSELECT1_QUICK1_HPP_
#define QUICKSELECT1_QUICK1_HPP_

int partition(float arreglo[][4], int p, int r);
void quick_select(float arreglo[][4], int p, int r, int k);


#endif /* QUICKSELECT1_QUICK1_HPP_ */
