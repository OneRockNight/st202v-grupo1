/*
 * Quick1.cpp
 *
 *  Created on: 18 abr. 2018
 *      Author: user
 */
#include<iostream>
#include<conio.h>
using namespace std;
#include "Quick1.hpp"


int partition(float arreglo[][4], int p, int r)
{
    float pivot = arreglo[r][2];

    while ( p < r )
    {
        while ( arreglo[p][2] < pivot )
            p++;

        while ( arreglo[r][2] > pivot )
            r--;

        if ( arreglo[p][2] == arreglo[r][2] )
            p++;
        else if ( p < r ) {
            float aux = arreglo[p][2];
            arreglo[p][2] = arreglo[r][2];
            arreglo[r][2] = aux;
        }
    }

    return r;
}

void quick_select(float arreglo[][4], int p, int r, int k)
{
    if ( p == r )
    	cout<< arreglo[p][2];
    int j = partition(arreglo, p, r);
    int length = j - p + 1;
    if ( length == k )
    	cout<< arreglo[j][2];
    else if ( k < length )
    	return quick_select(arreglo, p, j - 1, k);
    else  return quick_select(arreglo, j + 1, r, k - length);
}



