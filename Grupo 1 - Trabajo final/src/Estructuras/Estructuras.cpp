/*
 * Estructuras.cpp
 *
 *  Created on: 16 abr. 2018
 *      Author: Computer
 */
#include <iostream>
#include <string>
#include "Estructuras.hpp"
#include "../QuickSelect-mediana/mediana.hpp"

using namespace std;

float ZonasFiltradas[50000][4];

int Zona(string zona){
	int n;
	if (zona == "ORIENTE"){
		n = 0;
	}
	if (zona == "OCCIDENTE"){
		n = 1;
	}
	if (zona == "SUR"){
		n = 2;
	}
	return n;
}

string ZonaInv(float n){
	string s;
	if (n == 0){
		s = "ORIENTE";
	}
	if (n == 1){
		s = "OCCIDENTE";
	}
	if (n == 2){
		s = "SUR";
	}
	return s;
}

void Recorrer(float A[][4], int n, int max, int min, int B[3]){
	int cont = 0;
	cout << "FILTRADOS...";
	for (int i = 0; i < n; i++){
		int nz = (int)A[i][1];
		if (A[i][2] >= min && A[i][2] <= max && B[nz] == 1){
			cout << "N�mero de asiento: " << A[i][0] << " Zona: " << ZonaInv(A[i][1]) << " " << "Precio: " << A[i][2] << endl;
			cont++;
			ZonasFiltradas[cont][0] = A[i][0];
			ZonasFiltradas[cont][1] = A[i][1];
			ZonasFiltradas[cont][2] = A[i][2];
			ZonasFiltradas[cont][3] = A[i][3];
		}
	}
	cout << "RECOMENDACION: " << endl;
	medianaselect();
	cout << endl;
}

void IntroduccionDatos(float A[][4], int n){
	int chair, precio;
	string zona;
	for (int i = 0;  i < n; i++){
		cout << "Introduzca el numero de asiento" << endl;
		cin >> chair;
		A[i][0] = chair;
		cout << "Introduzca la zona del asiento" << endl;
		cout << "ORIENTE/OCCIDENTE/SUR" << endl;
		cin >> zona;
		A[i][1] = Zona(zona);
		cout << "Introduzca el precio" << endl;
		cin >> precio;
		A[i][2] = precio;
		A[i][3] = 0;
	}
}
