/*
 * Estructuras.hpp
 *
 *  Created on: 16 abr. 2018
 *      Author: Computer
 */

#ifndef ESTRUCTURAS_ESTRUCTURAS_HPP_
#define ESTRUCTURAS_ESTRUCTURAS_HPP_
#include <string>

using namespace std;

void IntroduccionDatos(float A[][4], int n);
void Recorrer(float A[][4], int n, int max, int min, int B[3]);
string ZonaInv(float n);

#endif /* ESTRUCTURAS_ESTRUCTURAS_HPP_ */
